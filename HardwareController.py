# -*- coding: utf-8 -*-
"""Base controller to interact with the Sofa scene.
"""

__authors__ = "emenager, tnavez"
__contact__ = "etienne.menager@inria.fr, tanguy.navez@inria.fr"
__version__ = "1.0.0"
__copyright__ = "(c) 2020, Inria"
__date__ = "Aug 2 2023"

# import Sofa
import utils
import numpy as np
import time as time

import polhemus_liberty.python.PolhemusUSB as tracking
import phidget1002_0b.Multiple_Actuators_Class as actuation

MEASURE_TIME = False

# Boolean for triggering different modes
CLOSED_LOOP = True # Wether to use closed or open loop
ERROR_CRITERION = True # Wether to use error or number of iteration as a criterion for moving in the next point of the trajectory
QP_FOR_SA = False # Wether to use a first QP to asses the mechanical state of the robot or not


class EmbeddedController():
    def __init__(self):
        """Classical initialization of a python class.

        Note:
        ----
            The kwargs argument must containe:
                - root: the root of the SOFA scene.
                - actuators: the actuators we want to control (force)
        """

        self.n_act_constraint = 6
        self.W0, self.dfree0, self.scaling, self.model, self.n_constraint = utils.create_network()
        self.problem = utils.init_QP(self.n_act_constraint)

        self.delta_a = None
        self.prev_s_a = None
        self.s_a = None

        self.step = 0

        # Variables for managing trajectory update
        self.waiting_time = 1 # Waiting time for the robot to reach equilibrium at the beginning of the simulation
        if ERROR_CRITERION:
            self.id_goal = 0
            self.stabilize_it = 0 
            self.max_stabilize_it = 10
            self.stabilize_timer = time.time()
        else:
            self.step_per_goal = 1 # Number of time steps between two goals      

        #Create trajectory
        self.get_trajectory()
        self.goal = self.goals[0]
        self.corrected_goal = self.goal

        #Create the PID for the correction
        self.PID = utils.PID(P=0.01, I=0.01, D=0.) 

        #Connect the hardware :
        self.tracker = tracking.PolhemusOvercoat(init_pos = [110,0,0],axis = [2,-1,0],print_flag = False) 
        self.actuators = actuation.Multiple_Actuators(print_flag = False)
        
        # Init data structures for registering results
        self.pos_effector_history = []
        self.pos_goal_history = []

        
    def get_trajectory(self):
        """Generate the trajectory we want to apply in the scene.

        Note:
        ----
            self.goals: list of 3D goals

        """
        # Init goals with equilibrium position
        self.goals = []
        # self.goals = [np.array([110.0, 0.0, 0.0])]
        
        # Circle at base
        # self.goals += utils.Circle(center = np.array([110, 0, 0]), radius = 20, 
        #                              n_samples = 500, steps_to_circle = 50)

        # Circle middle
        # self.goals += utils.Circle(center = np.array([110, 0, 0]), radius = 40, 
        #                              n_samples = 600, steps_to_circle = 100)
        
        # Medium circle at base
        # self.goals += utils.Circle(center = np.array([110, 0, 0]), radius = 30, 
        #                               ·n_samples = 300, steps_to_circle = 50)
        
        # # Small circle at base
        # self.goals += utils.Circle(center = np.array([110, 0, 0]), radius = 20, 
        #                               n_samples = 50, steps_to_circle = 50)
        
        # # # Reaching one target
        # K = 0 # Index of reached point
        # N_MAX = 5 # Discretization
        # angular_pos_target = K * 6.28319 / N_MAX
        # self.goals += utils.Traj_To_Target(102, 45, angular_pos_target, 50)

        # self.goals += [np.array([57, -5, -70])]
        # self.goals += [np.array([57, -35, 35])]
        # self.goals += [np.array([106, 26, 32.5])]
        self.goals += [np.array([92.5,-58.5,9])]

        
        # # 3D spiral trajectory
        # self.goals += utils.Spiral3D(center = np.array([110 - 0.25, 0, 0]), radius = 40, 
        #                             n_samples = 10000, steps_to_spiral = 100)
        
        # 3D inverse spiral trajectory
        # self.goals += utils.Spiral3D(center = np.array([110 - 10, 0, 0]), radius = 40, 
        #                             n_samples = 1000, steps_to_spiral = 100, is_inverse = True)
        

    def EachTimeStep(self):
        
        if self.step == self.waiting_time:
            #The first state is given by applying no force

            Waa0 =self.W0[:self.n_act_constraint, :self.n_act_constraint]
            dfreea0 = self.dfree0[:self.n_act_constraint]
            self.lambda_a = np.array([0 for _ in range(self.n_act_constraint)])
            self.s_a = (utils.compute_delta_a(self.lambda_a, Waa0, dfreea0)).tolist()
            self.prev_s_a = self.s_a

        if self.step >= self.waiting_time:

            ### Recover the true position of the effector
            [pos, q] = self.tracker.get_data()
            
            # Update goal depending on goal criterion
            if ERROR_CRITERION:
                is_goal_reached = utils.check_position_error(pos, np.array(self.goal), threshold = 0.5)
                if is_goal_reached:
                    self.id_goal += 1
                    self.goal = self.goals[min(self.id_goal, len(self.goals)-1)]      
                    # self.stabilize_it += 1
                    # if self.stabilize_it >= self.max_stabilize_it:
                    #     print("--------------------------------------------")
                    #     print("--------------------------------------------")
                    #     print("FINIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII")
                    #     print("Equilibrium reached for iter" + str(self.step))
                    #     print("--------------------------------------------")
                    #     print("--------------------------------------------")
                    if time.time() - self.stabilize_timer >= 2.4:
                        print("--------------------------------------------")
                        print("--------------------------------------------")
                        print("FINIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII")
                        print("Equilibrium reached for iter" + str(self.step))
                        print("--------------------------------------------")
                        print("--------------------------------------------")
                else:
                    self.stabilize_timer = time.time()
                         

            ### Correct the goal to take into account the difference between model and reality
            if CLOSED_LOOP:
                #self.corrected_goal+= 0.02 * (self.goal - np.array(pos, dtype='float64')) 
                self.corrected_goal = self.PID.compute_correction(pos, np.array(self.goal)) 
            else:
                self.corrected_goal = self.goal

            ### Recover the actual true position of actuator displacements
            if QP_FOR_SA:
                #Predict mechanical matrices from the network
                # Hypothesis: robot state close to the one from the previous assumption
                Waa, Wea, dfree_a, dfree_e = utils.predict_W_dFree(self.model, self.W0, self.dfree0, self.prev_s_a, self.scaling, self.n_constraint,
                                                                self.n_act_constraint, pos, nb_effector=1)

                #Compute optimization problem's matrices from the mechanical matrices
                H, g, A, lb, ub, lbA, ubA = utils.build_QP_system(self.lambda_a, Waa, Wea, dfree_a, dfree_e, delta_lambda_a = 20.0, use_epsilon=True)  

                #Compute the actual actuators' state from mechanical matrices and forces
                lambda_a = utils.solve_QP(self.problem, H, g, A, lb, ub, lbA, ubA, is_init=(self.step != self.waiting_time))
                self.s_a = (utils.compute_delta_a(self.lambda_a, Waa, dfree_a)).tolist()

            if MEASURE_TIME:
                start_time = time.time()

            ### Compute and apply the needed actuation displacement for reaching target goal
            # Predict mechanical matrices from the network
            Waa, Wea, dfree_a, dfree_e = utils.predict_W_dFree(self.model, self.W0, self.dfree0, self.s_a, self.scaling, self.n_constraint,
                                                               self.n_act_constraint, self.corrected_goal, nb_effector=1)

            # Compute optimization problem's matrices from the mechanical matrices
            H, g, A, lb, ub, lbA, ubA = utils.build_QP_system(self.lambda_a, Waa, Wea, dfree_a, dfree_e, delta_lambda_a = 20.0, use_epsilon=True)  

            # Solve the optimization problem to recover lambda_a, the force to apply
            self.lambda_a = utils.solve_QP(self.problem, H, g, A, lb, ub, lbA, ubA, is_init=(self.step != self.waiting_time))

            # Compute the new actuators' state from mechanical matrices and forces
            self.prev_s_a = self.s_a
            self.s_a = (utils.compute_delta_a(self.lambda_a, Waa, dfree_a)).tolist()

            # Apply the forces
            lambda_a = [self.lambda_a[i] * 10 for i in range(len(self.lambda_a)) ] # *10 car la simulation sur laquelle elle se base est dynamique ave dt = 0.1

            if MEASURE_TIME:
                end_time = time.time()
                print("Computation time for simulation step:", end_time - start_time)

            self.actuators.apply_pressure_tab(pressure_tab = lambda_a)

        self.step+=1

        ### Plot and manage trajectories
        if self.step >= self.waiting_time:
            # Manage the trajectory
            if not ERROR_CRITERION:
                self.goal = self.goals[min((self.step - self.waiting_time)//self.step_per_goal, len(self.goals)-1)]

            # Plot trajectory
            if self.step >= self.waiting_time+1:
                if ERROR_CRITERION:
                    # if self.stabilize_it < self.max_stabilize_it:
                    if time.time() - self.stabilize_timer <= 2.4:
                        print("self.step:", self.step)
                        print("-----------")
                        print("Position désirée")
                        print(self.goal)
                        print("Position corrigée")
                        print(self.corrected_goal)
                        print("Position mesurée")
                        print([round(pos[0],4),round(pos[1],4),round(pos[2],4) ])
                        print("Pressions applquées")
                        print(lambda_a)
                        print("-----------")

                else:
                    print("self.step:", self.step)
                    print("-----------")
                    print("Position désirée")
                    print(self.goal)
                    print("Position corrigée")
                    print(self.corrected_goal)
                    print("Position mesurée")
                    print([round(pos[0],4),round(pos[1],4),round(pos[2],4) ])
                    print("Pressions applquées")
                    print(lambda_a)
                    print("-----------")



                # Register results
                self.pos_effector_history.append(np.array([round(pos[0],4),round(pos[1],4),round(pos[2],4)]))
                self.pos_goal_history.append(self.goal)
                np.savetxt('pos_effector_history.csv', self.pos_effector_history, delimiter=',')
                np.savetxt('pos_goal_history.csv', self.pos_goal_history, delimiter=',')

            