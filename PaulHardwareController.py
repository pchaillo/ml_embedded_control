# -*- coding: utf-8 -*-
"""Base controller to interact with the Sofa scene.
"""

__authors__ = "emenager, tnavez"
__contact__ = "etienne.menager@inria.fr, tanguy.navez@inria.fr"
__version__ = "1.0.0"
__copyright__ = "(c) 2020, Inria"
__date__ = "Aug 2 2023"

# import Sofa
import utils
import numpy as np
import time as time

import polhemus_liberty.python.PolhemusUSB as tracking
import phidget1002_0b.Multiple_Actuators_Class as actuation

MEASURE_TIME = False



class EmbeddedController():
    def __init__(self):
        """Classical initialization of a python class.

        Note:
        ----
            The kwargs argument must containe:
                - root: the root of the SOFA scene.
                - actuators: the actuators we want to control (force)
        """
        # super(EmbeddedController, self).__init__(*args, **kwargs)

        # self.root = kwargs.get("root")
        # self.list_actuators = kwargs.get("list_actuators")
        # self.goalMO = kwargs.get("goalMO")
        # self.effectorMO = kwargs.get("effectorMO")

        self.n_act_constraint = 6
        self.W0, self.dfree0, self.scaling, self.model, self.n_constraint = utils.create_network()
        self.problem = utils.init_QP(self.n_act_constraint)

        self.delta_a = None
        self.s_a = None

        self.step = 0

        self.waiting_time = 1
        self.step_per_goal = 1

        #Create trajectory
        self.get_trajectory()
        self.goal = self.goals[0]

        #Create the PID for the correction
        self.PID = utils.PID(P=0.01, I=0.002, D=0.) # nouvelles valeurs ( P = 0.1 / I = 0.2 )

        #Connect the hardware :
        self.tracker = tracking.PolhemusOvercoat(init_pos = [110,0,0],axis = [2,-1,0],print_flag = False) 
        self.actuators = actuation.Multiple_Actuators(print_flag = False)

        
    def get_trajectory(self):
        """Generate the trajectory we want to apply in the scene.

        Note:
        ----
            self.goals: list of 3D goals

        """
        # self.goals = utils.SpiralPatternGenerator(x_zone_size=40,y_zone_size=40,plan_height = 110,point_number=12000)

        self.goals = []

        self.goals.append(np.array([110, 20, 20]))

        # self.goals.append(np.array([110, 5, 5]))

        # circle_center = np.array([110, 0, 0])
        # circle_radius = 30
        # n_samples = 20
        # for i in range(n_samples):
        #    self.goals.append(circle_center + np.array([0, circle_radius * np.cos(2 * np.pi * i / n_samples), 0]))
        
        # # 3D spiral trajectory
        # center= np.array([110 - 0.25, 0, 0])
        # radius = 40
        # n_samples = 10000
        # height_increment = 10.0 / n_samples
        # additional_height = 0.0
        # angle_increment = 2 * 6.28319 / n_samples
        # additional_angle = 0.0
        
        # # Starting goal pos to first goal pos on the trajectory
        # steps_to_spiral = 100
        # start_spiral_point = np.array([center[0] + additional_height,
        #                                center[1] + radius * np.cos(additional_angle),
        #                                center[2] + radius * np.sin(additional_angle)])
        # for i in range(steps_to_spiral):
        #     x = center[0] + i * (start_spiral_point[0] - center[0]) / steps_to_spiral
        #     y = center[1] + i * (start_spiral_point[1] - center[1]) / steps_to_spiral
        #     z = center[2] + i * (start_spiral_point[2] - center[2]) / steps_to_spiral
        #     self.goals.append(np.array([x,y,z]))
        
        # for i in range(n_samples - steps_to_spiral):
        #     x = center[0] - additional_height
        #     y = center[1] + radius * np.cos(additional_angle)
        #     z = center[2] + radius * np.sin(additional_angle)
        #     self.goals.append(np.array([x,y,z]))

        #     additional_height += height_increment
        #     additional_angle += angle_increment

    def EachTimeStep(self):
        
        if self.step == self.waiting_time:
            #The first state is given by applying no force

            Waa0 =self.W0[:self.n_act_constraint, :self.n_act_constraint]
            dfreea0 = self.dfree0[:self.n_act_constraint]
            self.lambda_a = np.array([0 for _ in range(self.n_act_constraint)])
            self.s_a = (utils.compute_delta_a(self.lambda_a, Waa0, dfreea0)).tolist()

        if self.step >= self.waiting_time:

            #Recover the true position of the effector
            [pos, q] = self.tracker.get_data()
            print("True position of the effector:", pos)

            # #Correct the goal to take into account the difference between model and reality
            # corrected_goal = self.PID.compute_correction(pos, self.goal) # comment for open loop
            corrected_goal = self.goal

            if MEASURE_TIME:
                start_time = time.time()

            #Predict mechanical matrices from the network
            Waa, Wea, dfree_a, dfree_e = utils.predict_W_dFree(self.model, self.W0, self.dfree0, self.s_a, self.scaling, self.n_constraint,
                                                               self.n_act_constraint, corrected_goal, nb_effector=1)

            #Compute optimization problem's matrices from the mechanical matrices
            H, g, A, lb, ub, lbA, ubA = utils.build_QP_system(self.lambda_a, Waa, Wea, dfree_a, dfree_e, delta_lambda_a = 10.0, use_epsilon=True)  

            #Solve the optimization problem to recover lambda_a, the force to apply
            self.lambda_a = utils.solve_QP(self.problem, H, g, A, lb, ub, lbA, ubA, is_init=(self.step != self.waiting_time))

            #Compute the new actuators' state from mechanical matrices and forces
            self.s_a = (utils.compute_delta_a(self.lambda_a, Waa, dfree_a)).tolist()
            print("Actuator state:", self.s_a)

            #Apply the forces
            lambda_a = [self.lambda_a[i]*20 for i in range(len(self.lambda_a)) ] # *10 car la simulation sur laquelle elle se base est dynamique ave dt = 0.1

            if MEASURE_TIME:
                end_time = time.time()
                print("Computation time for simulation step:", end_time - start_time)

            self.actuators.apply_pressure_tab(pressure_tab = lambda_a)

        self.step+=1

        #Manage the trajectory
        self.goal = self.goals[min((self.step - self.waiting_time)//self.step_per_goal, len(self.goals)-1)]
        
        if self.step >= self.waiting_time+1:
            if (self.step-self.waiting_time ) % self.step_per_goal == self.step_per_goal - 1:
                print("self.step:", self.step)
                print("-----------")
                print("Position désirée")
                print(self.goal)
                print("Position mesurée")
                print([round(pos[0],4),round(pos[1],4),round(pos[2],4) ])
                print("Pressions applquées")
                print(lambda_a)
                print("-----------")
            else:
                print("In progress ....")
                print("self.step:", self.step)
                print("-----------")
                print("Position désirée")
                print(self.goal)
                print("Position mesurée")
                print([round(pos[0],4),round(pos[1],4),round(pos[2],4) ])
                print("Pressions applquées")
                print(lambda_a)
                print("-----------")
