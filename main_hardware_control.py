import HardwareController as Controller
import time as time

Embedded_Object = Controller.EmbeddedController()

MEASURE_TIME = True

dt_max = 0.25 # Imposed time step

# while 1 :
# 	# input()
# 	if MEASURE_TIME:
# 		start_time = time.time()
# 	Embedded_Object.EachTimeStep()
# 	if MEASURE_TIME:
# 		end_time = time.time()
# 		dt =  end_time - start_time
# 		print("Computation time for simulation step:", dt)

# 		# Wait dt_max
# 		dt = max(0, dt_max - dt)
# 		time.sleep(dt) 
# 	else:
# 		time.sleep(dt_max)


time_count = 0
while 1 :
	# input()
	if MEASURE_TIME:
		start_time = time.time()
	Embedded_Object.EachTimeStep()
	if MEASURE_TIME:
		end_time = time.time()
		dt =  end_time - start_time
		print("Computation time for simulation step:", dt)
		time_count += dt 
		print("Total computation time so far:", time_count)